# Nextflow BIDS Recipes

This project simplifies using [Nextflow](https://www.nextflow.io/) for [BIDS](https://bids-specification.readthedocs.io/en/stable/) inputs.

See `./main.nf` for example usage.

> This is work in progress and we expect active development.

## BIDS support files

| File | Description |
| ---- | ----------- |
| `./nextflow.config` | Specifies BIDS-relevant parameters for the pipeline configuration. Currently: `--bids` for BIDS directory, `--sub` for comma-separated list of subject labels (all subjects if not specified), `--ses` for comma-separated list of session labels (all sessions if not specified). |
| `./bids_input_filter.nf` | Executes BIDS files indexing as documented in `./lib/BIDSIndex.groovy`. Filters the files based on command line arguments specified in `./nextflow.config`, and emits the surviving files in a channel.
| `./lib/BIDSIndex.groovy` | A custom Groovy class which implements BIDS file indexing. |
| `./main.nf` | Main pipeline file. Shows how to use the BIDS support files. |

## Pipeline parameters validation

We use [nf-validation](https://nextflow-io.github.io/nf-validation/) Nextflow plugin for validating pipeline parameters. It also helps with generating help messages. `./nextflow_schema.json` specifies the parameter details.

## Execution

Execute the latest version of this pipeline from the `main` branch of this repository:
```bash
./nextflow run -r main -latest https://gitlab.com/ccns/neurocog/neurodataops/anc/pipelines/nextflow-bids --bids testBIDS/ --sub v02
```
