// Importpipeline parameter validations and BIDS indexing
include { bidsInputFilter } from './bids_input_filter.nf'

// Dummy proces
process dummyProcess {
    input:
        val input
    
    output:
        stdout
    
    """
    echo -n "dummyProcess executed for subject ${input.sub} and file ${input.path}"
    """
}

workflow {
    // Get a channel with all BIDS input files that survived filtering based on command line parameters.
    bf = bidsInputFilter()

    /*
        EXAMPLE 1
     */
    
    // Filter out bold nii files
    bf.filter { it.suffix == 'bold' && (it.ext == 'nii' || it.ext == 'nii.gz') && it.echo != 'null' }
      .map { it -> [[it.sub, it.ses, it.echo] , it] } // Assign a unique key `[it.sub, it.ses, it.echo]` to each file
      .set { boldNii } // Assign name to the channel
    
    // Filter out bold json files
    bf.filter { it.suffix == 'bold' && it.ext == 'json' && it.echo != 'null' }
      .map { it -> [[it.sub, it.ses, it.echo] , it] } // Assign a unique key `[it.sub, it.ses, it.echo]` to each file
      .set { boldJSON } // Assign name to the channel

    // Join the nii and json files on the unique key, and view the output
    boldNii.join(boldJSON) | view

    /*
        EXAMPLE 2
     */
    
    // Filter out all T1w nii files, process them with `dummyProcess`, and view the output
    bf.filter { it.suffix == 'T1w' && (it.ext == 'nii' || it.ext == 'nii.gz') } | dummyProcess | view
}
